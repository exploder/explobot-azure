import logging
import os
import time

from azure.identity import DefaultAzureCredential
from azure.mgmt.containerservice import ContainerServiceClient
from azure.mgmt.containerservice.models import Code

from shared import Backoff, disable_http_logging


def start_cluster(container_client, resource_group, cluster_name):
    poller = container_client.managed_clusters.begin_start(
        resource_group, cluster_name)


def wait_until_success(container_client, resource_group, cluster_name):
    backoff = Backoff(max_exp=6)
    while True:
        cluster = \
            container_client.managed_clusters.get(resource_group, cluster_name)
        logging.info(f'Polling, current state: {cluster.provisioning_state}')
        if cluster.provisioning_state == 'Succeeded':
            return
        elif cluster.provisioning_state not in ['Stopping', 'Starting']:
            raise ValueError(f'Cluster in inconsistent state: '
                             f'{cluster.provisioning_state}')
        time.sleep(backoff.delay())


def main(unused) -> str:
    disable_http_logging()

    subscription = os.environ.get('AZURE_SUBSCRIPTION_ID')
    logging.info('Getting credential')
    credential = DefaultAzureCredential()
    res_grp = os.environ.get('AKS_RESOURCE_GROUP')
    cluster_name = os.environ.get('AKS_CLUSTER_NAME')

    container_client = ContainerServiceClient(credential, subscription)
    cluster = container_client.managed_clusters.get(res_grp, cluster_name)

    power_code = cluster.power_state.code
    prov_state = cluster.provisioning_state

    logging.info(f'Current power state: {power_code}')
    logging.info(f'Current provisioning state: {prov_state}')

    if prov_state == 'Succeeded':
        if power_code == Code.STOPPED:
            start_cluster(container_client, res_grp, cluster_name)
            logging.info('Cluster started!')
        else:
            logging.info('Cluster already started.')

    elif prov_state == 'Stopping':
        wait_until_success(container_client, res_grp, cluster_name)
        start_cluster(container_client, res_grp, cluster_name)
        logging.info('Cluster started!')

    elif prov_state == 'Starting':
        logging.info('Cluster starting already.')

    else:
        logging.error(f'Cluster in incorrect state: {prov_state}')
        raise ValueError(f'Cluster state inconsistent: {prov_state}')

    return 'success'
