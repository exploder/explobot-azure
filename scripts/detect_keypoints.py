import argparse

import numpy as np

import yaml
import torch
import imageio
import skimage.transform as sktr
from tqdm import tqdm, trange

from modules.keypoint_detector import KPDetector


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        type=str,
        required=True
    )
    parser.add_argument(
        '-o', '--output',
        type=str,
        required=True
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        required=True
    )
    parser.add_argument(
        '-C', '--checkpoints',
        type=str,
        required=True
    )
    parser.add_argument(
        '--cpu',
        action='store_true',
        required=False,
        default=False
    )
    parser.add_argument(
        '--best-frame',
        type=int,
        required=False,
        default=None
    )

    args = parser.parse_args()

    print('Loading checkpoints')
    with open(args.config, 'r') as f:
        config = yaml.safe_load(f)

    kp_detector = KPDetector(
        **config['model_params']['kp_detector_params'],
        **config['model_params']['common_params']
    )
    if args.cpu:
        checkpoints = torch.load(args.checkpoints, map_location=torch.device('cpu'))
    else:
        checkpoints = torch.load(args.checkpoints)
        kp_detector.cuda()

    kp_detector.load_state_dict(checkpoints['kp_detector'])
    kp_detector.eval()
    kp_detector.zero_grad(set_to_none=True)

    print('Reading driving video')
    reader = imageio.get_reader(args.input)
    fps = reader.get_meta_data()['fps']
    driving_video = []
    for image in reader:
        driving_video.append(image)
    reader.close()

    print('Resizing reading video')
    resized_frames = []
    for frame in tqdm(driving_video):
        resized_frames.append(
            sktr.resize(
                frame,
                (256, 256),
                anti_aliasing=False,
                mode='constant'
            )[..., :3]
        )

    driving = torch.tensor(
        np.array(resized_frames)[np.newaxis].astype(np.float32)
    ).permute(0, 4, 1, 2, 3)

    print('Detecting keypoints')
    keypoints = []
    with torch.no_grad():
        for idx in trange(driving.shape[2]):
            frame = driving[:, :, idx]
            kp = kp_detector(frame if args.cpu else frame.cuda())
            keypoints.append(kp)

    video_dict = {
        'keypoints': keypoints,
        'fps': fps,
        'best_frame': args.best_frame
    }
    torch.save(video_dict, args.output)


if __name__ == '__main__':
    main()
