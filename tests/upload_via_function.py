import asyncio

import aiohttp


async def upload():
    async with aiohttp.ClientSession() as ses:
        url = 'http://127.0.0.1:7071/api/start-gpu-processing'
        files = {'file': open('e_crop.png', 'rb')}
        async with ses.post(url, params={'skip_cluster': 'true'}, data=files) as resp:
            print(await resp.json())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(upload())
