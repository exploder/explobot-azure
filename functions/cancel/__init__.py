import logging
import os

import azure.functions as func
from azure.data.tables import TableClient
from azure.core.exceptions import HttpResponseError


INCORRECT_STATE = 'INCORRECT_STATE'


def main(req: func.HttpRequest) -> func.HttpResponse:
    job_id = req.params.get('job')
    if job_id is None:
        return func.HttpResponse(status_code=400)

    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    table_name = os.environ.get('EXPLOBOT_STATUS_TABLE')
    status_table = TableClient.from_connection_string(conn_str, table_name)

    try:
        entity = status_table.get_entity('explobot-gpu', job_id)
    except HttpResponseError:
        logging.warning('No entities')
        return func.HttpResponse(status_code=404)

    if entity['status'] != 'queued':
        logging.warning(f'Incorrect state for entity {entity}')
        return func.HttpResponse(INCORRECT_STATE, status_code=400)

    entity['status'] = 'cancelled'
    status_table.update_entity(entity)

    return func.HttpResponse(status_code=200)
