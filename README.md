Everything except `fom_helper.py` and `fom_processor.py` in the `gpu-processor-docker` directory is licensed with the AGPLv3 license.
Those two files are licensed with the Creative Commons Attribution-NonCommercial 4.0 International license because they are heavily based on the First Order Model by Aliaksandr Siarohin, and that project is licensed with the CC license.
See LICENSE-CC.md for details on the second license.
