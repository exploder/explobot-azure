import os
import json

import azure.functions as func
from azure.storage.queue import QueueClient, TextBase64EncodePolicy, \
    TextBase64DecodePolicy


def main(req: func.HttpRequest) -> func.HttpResponse:
    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    queue_name = os.environ.get('EXPLOBOT_JOB_QUEUE')

    job_queue = QueueClient.from_connection_string(
        conn_str,
        queue_name,
        message_decode_policy=TextBase64DecodePolicy(),
        message_encode_policy=TextBase64EncodePolicy()
    )

    job_list = []
    for msg in job_queue.peek_messages(max_messages=32):
        job_list.append(msg.content)

    return func.HttpResponse(
        body=json.dumps(job_list),
        status_code=200,
        mimetype='application/json'
    )
