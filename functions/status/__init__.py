import re
import os
import json
import logging

import azure.functions as func
from azure.identity import DefaultAzureCredential
from azure.data.tables import TableClient
from azure.mgmt.containerservice import ContainerServiceClient

from shared import disable_http_logging


UUID_REGEX = re.compile(
    r'^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}$'
)


def main(req: func.HttpRequest) -> func.HttpResponse:
    disable_http_logging()

    param = req.params.get('jobs')
    if param is None:
        return func.HttpResponse(status_code=400)

    potential_uuids = param.split(',')
    uuids = []
    for pot_uuid in potential_uuids:
        if UUID_REGEX.match(pot_uuid):
            uuids.append(pot_uuid)
        else:
            logging.warning(f'Illegal UUID: {pot_uuid}')

    filter_str = ' or '.join([f'RowKey eq @{i}' for i in range(len(uuids))])
    params = {f'{i}': uuids[i] for i in range(len(uuids))}

    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    table_name = os.environ.get('EXPLOBOT_STATUS_TABLE')
    status_table = TableClient.from_connection_string(conn_str, table_name)

    results = status_table.query_entities(
        query_filter=filter_str,
        parameters=params
    )

    statuses = []
    for result in results:
        statuses.append({
            'jobid': result['RowKey'],
            'status': result['status']
        })

    subscription = os.environ.get('AZURE_SUBSCRIPTION_ID')
    logging.info('Getting credential')
    credential = DefaultAzureCredential()
    res_grp = os.environ.get('AKS_RESOURCE_GROUP')
    cluster_name = os.environ.get('AKS_CLUSTER_NAME')

    container_client = ContainerServiceClient(credential, subscription)
    cluster = container_client.managed_clusters.get(res_grp, cluster_name)

    power_code = cluster.power_state.code
    prov_state = cluster.provisioning_state

    cluster_state = {
        'power': power_code,
        'state': prov_state
    }

    data = {
        'jobs': statuses,
        'cluster': cluster_state
    }

    return func.HttpResponse(
        body=json.dumps(data),
        status_code=200,
        mimetype='application/json'
    )
