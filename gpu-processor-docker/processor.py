import logging
import sys
from io import BytesIO


class Processor:
    def __init__(self, processor_name='processor'):
        self.logger = logging.getLogger(processor_name)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s'
        )
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def load_model(self):
        raise NotImplementedError

    def unload_model(self):
        raise NotImplementedError

    def is_loaded(self) -> bool:
        raise NotImplementedError

    def load_other_data(self):
        pass

    def process(
            self, input_data: BytesIO, params: dict, logger=None
    ) -> BytesIO:
        raise NotImplementedError
