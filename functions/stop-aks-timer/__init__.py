import os
import logging
from datetime import datetime, timezone, timedelta

import azure.functions as func
import requests
from azure.identity import DefaultAzureCredential
from azure.mgmt.containerservice import ContainerServiceClient
from azure.mgmt.containerservice.models import Code
from azure.storage.queue import QueueClient
from azure.data.tables import TableClient

from shared import disable_http_logging


def main(akstimer: func.TimerRequest) -> None:
    if akstimer.past_due:
        logging.info('The timer is past due!')

    disable_http_logging()
    # Disable the warning that get_token failed
    logging\
        .getLogger('azure.identity._internal.interactive')\
        .setLevel(logging.ERROR)

    credential = DefaultAzureCredential()
    res_grp = os.environ.get('AKS_RESOURCE_GROUP')
    cluster_name = os.environ.get('AKS_CLUSTER_NAME')

    subscription = os.environ.get('AZURE_SUBSCRIPTION_ID')

    container_client = ContainerServiceClient(credential, subscription)
    cluster = container_client.managed_clusters.get(res_grp, cluster_name)

    power_code = cluster.power_state.code
    prov_state = cluster.provisioning_state

    if power_code == Code.STOPPED:
        logging.info('Cluster already stopped, not stopping')
        return

    if prov_state == 'Failed':
        logging.warning('Current provisioning state is Failed, will attempt to stop if other criteria are satisfied!')
        tg_token = os.environ.get('TELEGRAM_BOT_TOKEN')
        tg_chat = os.environ.get('TELEGRAM_CHAT_ID')
        resp = requests.post(
            f'https://api.telegram.org/bot{tg_token}/sendMessage',
            json={
                'chat_id': int(tg_chat),
                'text': f'Cluster state:\nProvisioning state: {prov_state}\nPower state: {power_code}',
                'disable_notification': True,
            },
        )
        logging.info(f'Sent Telegram notification, got response code {resp.status_code}')
    elif prov_state != 'Succeeded':
        logging.info(f'Current provisioning state {prov_state}, not stopping')
        return

    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    queue_name = os.environ.get('EXPLOBOT_JOB_QUEUE')

    job_queue = QueueClient.from_connection_string(conn_str, queue_name)
    msg_count = job_queue.get_queue_properties().approximate_message_count

    if int(msg_count) > 0:
        logging.info('Messages found in job queue, not stopping')
        return

    table_name = os.environ.get('EXPLOBOT_STATUS_TABLE')
    status_table = TableClient.from_connection_string(conn_str, table_name)

    tdiff = datetime.now(timezone.utc) - timedelta(minutes=10)
    results = status_table.query_entities(
        query_filter=f'Timestamp ge @time',
        parameters={'time': tdiff}
    )
    try:
        next(results)
        logging.info('Recent results in ready table, not stopping')
        return
    except StopIteration:
        # Continue
        logging.info('No new items in ready table')

    logging.info('Stopping the cluster!')
    container_client.managed_clusters.begin_stop(res_grp, cluster_name)
