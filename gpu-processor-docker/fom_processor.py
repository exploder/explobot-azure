import json
import torch
from io import BytesIO

import imageio
import skimage
import ffmpeg
import skimage.transform as sktr
from PIL import Image
from azure.data.tables import EntityProperty

from processor import Processor
from fom_helper import load_checkpoints, make_animation


class FOMProcessor(Processor):
    def __init__(self, config_path, checkpoint_path):
        super().__init__('first-order-model')
        self._config_path = config_path
        self._checkpoint_path = checkpoint_path
        self._kp_data = []
        self._video_paths = []
        self._generator = None
        self._kp_detector = None

    def load_model(self):
        self.logger.info('Reading checkpoints')
        self._generator, self._kp_detector = \
            load_checkpoints(self._config_path, self._checkpoint_path)

    def unload_model(self):
        del self._generator
        del self._kp_detector
        self._generator = None
        self._kp_detector = None

    def is_loaded(self) -> bool:
        return self._generator is not None and self._kp_detector is not None

    def load_other_data(self):
        self.logger.info('Reading driving videos')

        with open('/files/videos/videos.json', 'r') as json_file:
            video_config = json.load(json_file)

        for driving_video_info in video_config:
            self._kp_data.append(torch.load(driving_video_info['keypoints']))
            self._video_paths.append(driving_video_info['video'])

    def process(
            self, input_data: BytesIO, params: dict, job_logger=None
    ) -> BytesIO:
        driving_id = params.get('driving_id', 0)
        # Newer azure function versions or smth set it to INT64 which causes this to be converted
        # as an EntityProperty instead of basic int
        if isinstance(driving_id, EntityProperty):
            driving_id = int(driving_id.value)
        video_uuid = params.get('video_uuid')

        input_data.seek(0)
        mode_changed = False

        with Image.open(input_data) as tmp_img:
            if tmp_img.mode not in ['RGB', 'RGBA']:
                mode_changed = True
                job_logger.warning(
                    f'Converting from {tmp_img.mode} to RGBA!'
                )
                new_img = tmp_img.convert('RGBA')
                new_bytes = BytesIO()
                new_img.save(new_bytes, format='PNG')
                new_bytes.seek(0)
                fmt = 'PNG'
            else:
                fmt = tmp_img.format
        job_logger.info(f'Image format is {fmt}')
        input_data.seek(0)

        if mode_changed:
            source_image = imageio.imread(new_bytes, format=fmt)
            new_bytes.close()
        else:
            source_image = imageio.imread(input_data, format=fmt)

        source_image = sktr.resize(
            source_image,
            (256, 256),
            anti_aliasing=False,
            mode='constant'
        )[..., :3]

        driving_data = self._kp_data[driving_id]

        job_logger.info(f'Best frame: {driving_data["best_frame"]}')

        predictions = make_animation(
            source_image=source_image,
            driving_kp=driving_data['keypoints'],
            generator=self._generator,
            kp_detector=self._kp_detector,
            logger=job_logger,
            best_frame=driving_data['best_frame']
        )

        job_logger.info('Saving video')
        imageio.mimsave(
            uri=f'/tmp/{video_uuid}.mp4',
            ims=[skimage.img_as_ubyte(frame) for frame in predictions],
            fps=driving_data['fps'],
            format='ffmpeg',
            output_params=['-f', 'mp4']
        )

        job_logger.info('Adding audio to video')
        video_stream = ffmpeg.input(f'/tmp/{video_uuid}.mp4').video
        audio_stream = ffmpeg.input(self._video_paths[driving_id]).audio
        ffmpeg.output(
            video_stream,
            audio_stream,
            f'/tmp/{video_uuid}_audio.mp4',
            c='copy'
        ).global_args('-hide_banner', '-loglevel', 'error').run()
        with open(f'/tmp/{video_uuid}_audio.mp4', 'rb') as video_file:
            output_bytes = BytesIO(video_file.read())
        return output_bytes
