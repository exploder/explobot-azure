import enum
import os
import random

from azure.identity import DefaultAzureCredential
from azure.storage.queue import (
    QueueClient,
    TextBase64DecodePolicy,
    TextBase64EncodePolicy
)
from azure.storage.blob import ContainerClient
from azure.data.tables import TableClient


class Backoff:
    """
    Implements a helper class for exponential backoff. Based on discord.py's MIT
    licensed implementation
    (https://github.com/Rapptz/discord.py/blob/9b4e820bbe432081b59936975202853455c938a4/discord/backoff.py)
    """
    def __init__(self, max_exp=12):
        self._exp = 0
        self._max_exp = max_exp
        self._random = random.Random()
        self._random.seed()

    def delay(self):
        self._exp = min(self._exp + 1, self._max_exp)
        return self._random.uniform(0, 2 ** self._exp)

    def reset(self):
        self._exp = 0


class AzureManager:
    def __init__(self, storage_account: str, connection_string: str):
        self._storage_account = storage_account
        self._connection_string = connection_string
        self._credential = DefaultAzureCredential()

    def queue(self, queue_name: str) -> QueueClient:
        storage_url = f'https://{self._storage_account}.queue.core.windows.net'
        return QueueClient(
            storage_url,
            queue_name,
            self._credential,
            message_decode_policy=TextBase64DecodePolicy(),
            message_encode_policy=TextBase64EncodePolicy()
        )

    def container_client(self, container_name: str) -> ContainerClient:
        storage_url = f'https://{self._storage_account}.blob.core.windows.net'
        return ContainerClient(storage_url, container_name, self._credential)

    def table_client(self, table_name: str) -> TableClient:
        return TableClient.from_connection_string(
            self._connection_string, table_name)


class JobStatus(enum.Enum):
    QUEUED = 'queued'
    PROCESSING = 'processing'
    DONE = 'ready'
    ERROR = 'error'
