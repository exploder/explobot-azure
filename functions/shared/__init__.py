import logging
import random


class Backoff:
    """
    Implements a helper class for exponential backoff. Based on discord.py's MIT
    licensed implementation
    (https://github.com/Rapptz/discord.py/blob/9b4e820bbe432081b59936975202853455c938a4/discord/backoff.py)
    """
    def __init__(self, max_exp=12):
        self._exp = 0
        self._max_exp = max_exp
        self._random = random.Random()
        self._random.seed()

    def delay(self):
        self._exp = min(self._exp + 1, self._max_exp)
        return self._random.uniform(0, 2 ** self._exp)


def disable_http_logging():
    http_logger = \
        logging.getLogger('azure.core.pipeline.policies.http_logging_policy')
    http_logger.setLevel(logging.WARNING)
