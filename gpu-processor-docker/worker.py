"""
Worker script that runs indefinitely and polls an Azure Storage Queue. Any
messages are processed.
"""


import os
import uuid
import time
import logging
import sys
from io import BytesIO

from azure.storage.queue import QueueMessage
from azure.data.tables import UpdateMode

from fom_processor import FOMProcessor
from utils import Backoff, AzureManager, JobStatus


def _logger_for(job_id: str) -> logging.Logger:
    formatter = logging.Formatter(
        '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
    stdout = logging.StreamHandler(sys.stdout)
    stdout.setFormatter(formatter)
    file = logging.FileHandler(f'/tmp/{job_id}.log')
    file.setFormatter(formatter)
    logger = logging.getLogger(job_id)
    logger.setLevel(logging.INFO)
    logger.addHandler(stdout)
    logger.addHandler(file)

    return logger


PROCESSOR_PARAMS = {
    'first-order-model': {
        'config_path': '/files/config/vox-256.yaml',
        'checkpoint_path': '/files/checkpoints/vox-cpk.pth.tar'
    },
    'vqgan-clip': {

    }
}


class Worker:
    def __init__(self):
        self._worker_logger = logging.getLogger('worker')
        self._worker_logger.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(formatter)
        self._worker_logger.addHandler(handler)
        self._audio_paths = []
        self._backoff = Backoff(max_exp=9)

        self._azure = AzureManager(
            os.environ.get('STORAGE_ACCOUNT'),
            os.environ.get('STORAGE_CONNECTION')
        )
        self._job_queue = self._azure.queue(os.environ.get('JOB_QUEUE'))

        self._error_queue = self._azure.queue(os.environ.get('ERROR_QUEUE'))

        self._job_container = \
            self._azure.container_client(os.environ.get('JOB_BLOB'))
        self._ready_container = \
            self._azure.container_client(os.environ.get('READY_BLOB'))

        self._log_container = \
            self._azure.container_client(os.environ.get('LOG_BLOB'))

        self._status_table = \
            self._azure.table_client(os.environ.get('STATUS_TABLE'))

        self._processors = {
            'first-order-model':
                FOMProcessor(**PROCESSOR_PARAMS['first-order-model']),
        }
        self._processors['first-order-model'].load_other_data()

        self._worker_logger.info('Init done!')

    def start(self):
        self._loop()

    def _update_job_status(self, job_uuid: str, status: JobStatus, **kwargs):
        entity = self._status_table.get_entity('explobot-gpu', job_uuid)
        entity['status'] = status.value
        for key in kwargs:
            entity[key] = kwargs[key]
        self._status_table.update_entity(entity, UpdateMode.REPLACE)

    def _get_driving_id(self, job_uuid: str):
        entity = self._status_table.get_entity('explobot-gpu', job_uuid)
        return entity.get('drivingVideo', 0)

    def _is_cancelled(self, job_uuid: str):
        entity = self._status_table.get_entity('explobot-gpu', job_uuid)
        return entity['status'] == 'cancelled'

    def _loop(self):
        while True:
            msg_count = \
                self._job_queue.get_queue_properties().approximate_message_count
            if int(msg_count) == 0:
                self._backoff_sleep()
                continue

            messages = self._job_queue.receive_messages(visibility_timeout=200)
            for message in messages:
                self._process_message(message)

            self._backoff.reset()

    def _backoff_sleep(self):
        delay = self._backoff.delay()
        self._worker_logger.debug(f'Sleeping {10:.2f} seconds')
        time.sleep(10)

    def _process_message(self, message: QueueMessage):
        blob_uuid = message.content
        job_logger = _logger_for(blob_uuid)
        if self._is_cancelled(blob_uuid):
            self._worker_logger.info(f'Job {blob_uuid} cancelled, skipping.')
            self._job_queue.delete_message(message)
            input_blob = self._job_container.get_blob_client(blob_uuid)
            input_blob.delete_blob(delete_snapshots='include')
            return
        try:
            self._update_job_status(blob_uuid, JobStatus.PROCESSING)
            processor = self._processors['first-order-model']
            if not processor.is_loaded():
                processor.load_model()

            driving_id = self._get_driving_id(blob_uuid)
            output_uuid = str(uuid.uuid4())
            output_blob = self._ready_container.get_blob_client(output_uuid)
            while output_blob.exists():
                output_uuid = str(uuid.uuid4())
                output_blob = self._ready_container.get_blob_client(output_uuid)

            job_logger.info(f'Downloading blob with UUID={blob_uuid}')
            input_blob = self._job_container.get_blob_client(blob_uuid)
            stream = input_blob.download_blob()

            with BytesIO() as input_bytes:
                input_bytes.write(stream.content_as_bytes())
                output_bytes = processor.process(
                    input_bytes,
                    {
                        'driving_id': driving_id,
                        'video_uuid': output_uuid
                    },
                    job_logger
                )

            output_bytes.seek(0)
            job_logger.info(f'Uploading blob with UUID={output_uuid} and '
                            f'finalizing queues')
            output_blob.upload_blob(output_bytes)
            output_bytes.close()

            self._job_queue.delete_message(message)
            input_blob.delete_blob(delete_snapshots='include')
            job_logger.info('Done')

            self._update_job_status(
                blob_uuid,
                JobStatus.DONE,
                video_uuid=output_uuid
            )
        except Exception:
            job_logger.exception('Exception while processing', exc_info=True)
            self._update_job_status(
                blob_uuid,
                JobStatus.ERROR
            )
            self._job_queue.delete_message(message)
            self._error_queue.send_message(blob_uuid)

        self._upload_log(blob_uuid)

    def _upload_log(self, job_id: str):
        self._worker_logger.info(f'Uploading log for {job_id}')
        log_blob = self._log_container.get_blob_client(f'{job_id}.log')
        with open(f'/tmp/{job_id}.log', 'r') as log_file:
            log_blob.upload_blob(log_file.read(), overwrite=True)
        self._worker_logger.info(f'Uploaded log for {job_id}')


def start_worker():
    worker = Worker()

    worker.start()


if __name__ == '__main__':
    start_worker()
