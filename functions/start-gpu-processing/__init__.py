import os
import json
import logging
import uuid

import azure.functions as func
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient, BlobClient

from shared import disable_http_logging


async def main(
        req: func.HttpRequest,
        starter: str,
        msg: func.Out[func.QueueMessage],
        entity: func.Out[str]) -> func.HttpResponse:
    disable_http_logging()

    file = req.files.get('file', None)
    if file is None:
        return func.HttpResponse(status_code=400)
    try:
        driving_video = int(req.params.get('driving_video', 0))
    except ValueError:
        return func.HttpResponse(status_code=400)
    max_driving_id = int(os.environ.get('MAX_DRIVING_VIDEO'))
    if driving_video < 0 or driving_video > max_driving_id:
        return func.HttpResponse(status_code=400)

    skip_cluster = req.params.get('skip_cluster', False)

    # The connection string will be in the environment variables
    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    container_name = os.environ.get('EXPLOBOT_IMAGE_CONTAINER')

    blob_service: BlobServiceClient = \
        BlobServiceClient.from_connection_string(conn_str)

    fname = str(uuid.uuid4())
    blob_client: BlobClient = blob_service.get_blob_client(
        container=container_name,
        blob=fname
    )

    # Make sure that the file is unique
    while blob_client.exists():
        fname = str(uuid.uuid4())
        blob_client: BlobClient = blob_service.get_blob_client(
            container=container_name,
            blob=fname
        )

    logging.info(f'Uploading to {fname}.')
    blob_client.upload_blob(file)
    msg.set(fname)

    entity_data = {
        'rowKey': fname,
        'status': 'queued',
        'drivingVideo': driving_video
    }
    entity.set(json.dumps(entity_data))

    client = df.DurableOrchestrationClient(starter)
    input_data = {
        'skip_cluster': skip_cluster
    }
    instance_id = await \
        client.start_new('bootstrap-orchestrator', None, json.dumps(input_data))

    logging.info(f"Started orchestration with ID = '{instance_id}'.")

    return func.HttpResponse(
        body=fname,
        status_code=200
    )
