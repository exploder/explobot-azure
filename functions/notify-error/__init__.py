import logging
import json
import os
import io

import requests
import azure.functions as func
from azure.storage.blob import BlobServiceClient, BlobClient

from shared import disable_http_logging


def main(msg: func.QueueMessage):
    disable_http_logging()

    image_uuid = msg.get_body().decode('utf-8')
    logging.info(f'Notifying error for {image_uuid}!')

    # The connection string will be in the environment variables
    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    image_container = os.environ.get('EXPLOBOT_IMAGE_CONTAINER')
    log_container = os.environ.get('EXPLOBOT_LOG_CONTAINER')

    blob_service: BlobServiceClient = BlobServiceClient.from_connection_string(conn_str)

    image_blob: BlobClient = blob_service.get_blob_client(container=image_container, blob=image_uuid)
    image_data = None
    if image_blob.exists():
        logging.info('Image file exists, downloading')
        image_data = io.BytesIO()
        image_blob.download_blob().readinto(image_data)
        image_data.seek(0)
    else:
        logging.info('Image file does not exist :(')

    log_blob: BlobClient = blob_service.get_blob_client(container=log_container, blob=f'{image_uuid}.log')
    log_data = None
    if log_blob.exists():
        logging.info('Log file exists, downloading')
        log_data = io.BytesIO()
        log_blob.download_blob().readinto(log_data)
        log_data.seek(0)
    else:
        logging.info('Log file does not exist :(')

    tg_token = os.environ.get('TELEGRAM_BOT_TOKEN')
    tg_chat = os.environ.get('TELEGRAM_CHAT_ID')

    msg_text = f'GPU processing failed for job ID={image_uuid}!'

    if image_data is not None or log_data is not None:
        files = {}
        if image_data is not None:
            files[f'{image_uuid}'] = image_data
        if log_data is not None:
            files[f'{image_uuid}.log'] = log_data

        media_list = []
        if image_data is not None:
            if log_data is None:
                media_list.append({'type': 'document', 'media': f'attach://{image_uuid}', 'caption': msg_text})
            else:
                media_list.append({'type': 'document', 'media': f'attach://{image_uuid}'})
        if log_data is not None:
            media_list.append({'type': 'document', 'media': f'attach://{image_uuid}.log', 'caption': msg_text})

        resp = requests.post(
            f'https://api.telegram.org/bot{tg_token}/sendMediaGroup',
            data={
                'chat_id': int(tg_chat),
                'media': json.dumps([
                    {'type': 'document', 'media': f'attach://{image_uuid}'},
                    {
                        'type': 'document',
                        'media': f'attach://{image_uuid}.log',
                        'caption': f'GPU processing failed for job {image_uuid}'
                    }
                ]),
                'disable_notification': True,
            },
            files={
                f'{image_uuid}': image_data,
                f'{image_uuid}.log': log_data,
            },
        )
    else:
        resp = requests.post(
            f'https://api.telegram.org/bot{tg_token}/sendMessage',
            json={
                'chat_id': int(tg_chat),
                'text': msg_text,
                'disable_notification': True,
            },
        )
    logging.info(f'Sent Telegram notification, got response code {resp.status_code}')
