"""
Creates animations from source image and driving video. Based heavily on the
demo.py file in https://github.com/AliaksandrSiarohin/first-order-model.
"""


import logging

import yaml
import torch
import numpy as np
from modules.generator import OcclusionAwareGenerator
from modules.keypoint_detector import KPDetector
from modules.util import normalize_kp


def load_checkpoints(config_path, checkpoint_path, cpu=False):
    with open(config_path) as f:
        config = yaml.safe_load(f)

    generator = OcclusionAwareGenerator(
        **config['model_params']['generator_params'],
        **config['model_params']['common_params']
    )

    if not cpu:
        generator.cuda()

    kp_detector = KPDetector(
        **config['model_params']['kp_detector_params'],
        **config['model_params']['common_params']
    )
    if not cpu:
        kp_detector.cuda()

    if cpu:
        checkpoint = torch.load(
            checkpoint_path,
            map_location=torch.device('cpu')
        )
    else:
        checkpoint = torch.load(checkpoint_path)

    generator.load_state_dict(checkpoint['generator'])
    kp_detector.load_state_dict(checkpoint['kp_detector'])

    generator.eval()
    kp_detector.eval()
    generator.zero_grad(set_to_none=True)
    kp_detector.zero_grad(set_to_none=True)

    return generator, kp_detector


def make_animation(
        source_image,
        driving_kp,
        generator,
        kp_detector,
        relative=True,
        adapt_movement_scale=True,
        cpu=False,
        logger=None,
        best_frame=None
):
    prev_cudnn_benchmark_value = torch.backends.cudnn.benchmark
    # ~10 % speedup in inference, at least on a GTX 1060 (6GB)
    torch.backends.cudnn.benchmark = True

    if best_frame is None:
        best_frame = 0

    logger = logger or logging.getLogger('job')
    with torch.no_grad():
        predictions = []
        source = torch.tensor(
            source_image[np.newaxis].astype(np.float32)
        ).permute(0, 3, 1, 2)

        if not cpu:
            source = source.cuda()

        kp_source = kp_detector(source)
        kp_driving_initial = driving_kp[best_frame]

        frame_count = len(driving_kp)

        for frame_idx in range(frame_count):
            if (frame_idx + 1) % 100 == 0:
                logger.info(f'Processing frame {frame_idx + 1}/{frame_count}')
            kp_driving = driving_kp[frame_idx]
            kp_norm = normalize_kp(
                kp_source=kp_source,
                kp_driving=kp_driving,
                kp_driving_initial=kp_driving_initial,
                use_relative_movement=relative,
                use_relative_jacobian=relative,
                adapt_movement_scale=adapt_movement_scale
            )
            out = generator(source, kp_source=kp_source, kp_driving=kp_norm)

            predictions.append(np.transpose(
                out['prediction'].data.cpu().numpy(), [0, 2, 3, 1]
            )[0])

    torch.backends.cudnn.benchmark = prev_cudnn_benchmark_value
    return predictions
