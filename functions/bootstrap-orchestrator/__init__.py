import json
import logging

import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    input_data = json.loads(context.get_input())

    if input_data['skip_cluster']:
        logging.info('Skipping cluster startup!')
    else:
        yield context.call_activity('start-aks-if-stopped')


main = df.Orchestrator.create(orchestrator_function)
