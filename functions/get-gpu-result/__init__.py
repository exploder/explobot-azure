import os
import logging

import azure.functions as func
from azure.storage.blob import BlobClient, StorageStreamDownloader
from azure.data.tables import TableClient
from azure.core.exceptions import HttpResponseError

from shared import disable_http_logging


def main(req: func.HttpRequest) -> func.HttpResponse:
    disable_http_logging()

    job_id = req.params.get('job', None)
    if job_id is None:
        logging.info('No jobid')
        return func.HttpResponse(status_code=400)

    conn_str = os.environ.get('EXPLOBOT_STORAGE')
    table_name = os.environ.get('EXPLOBOT_STATUS_TABLE')
    status_table = TableClient.from_connection_string(conn_str, table_name)

    try:
        entity = status_table.get_entity('explobot-gpu', job_id)
    except HttpResponseError:
        logging.warning('No entities')
        return func.HttpResponse(status_code=400)

    if entity['status'] != 'ready':
        return func.HttpResponse(status_code=400)

    video_uuid = entity['video_uuid']
    container_name = os.environ.get('EXPLOBOT_VIDEO_CONTAINER')
    ready_blob: BlobClient = BlobClient.from_connection_string(
        conn_str=conn_str,
        container_name=container_name,
        blob_name=video_uuid
    )
    stream: StorageStreamDownloader = ready_blob.download_blob()

    return func.HttpResponse(
        body=stream.readall(),
        mimetype='video/mp4',
        headers={'Content-Disposition': 'attachment; filename="video.mp4"'},
        status_code=200
    )
